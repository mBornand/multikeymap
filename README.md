# multikeymap
A map with multiple keys pointing to one value

This crate internally uses `std::collections::HashMap` and is aproximately a wrapper to `HashMap<K, (V, HashSet<K>)>`.

## Example
```
use multikeymap::MultiKeyMap;

fn example() {
    let set1: [usize; 4] = [1, 2, 3, 6];
    let set2: [usize; 4] = [5, 10, 20, 30];

    let mut map = MultiKeyMap::<usize, String>::new();
    map.insert(set1, "in set 1".to_string());
    map.insert(set2, "in set 2".to_string());

    for i in &set1 {
        assert!(map.get(i) == Some("in set 1".to_string()));
    }

    for i in &set2 {
        assert!(map.get(i) == Some("in set 2".to_string()));
    }
}
```
## Features
- `serde` derives Serialize and Deserialize

## Status
For now it is not stable nor complete

## License
Licensed at your option either:
- MIT License ([LICENSE-MIT](LICENSE-MIT))
or
- Apache 2.0 License ([LICENSE-APACHE](LICENSE-APACHE))
