use core::hash::Hash;
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};
use std::{
    collections::{hash_map, HashMap, HashSet},
    sync::{Arc, RwLock},
};

///entry of `MultikeyMap`
#[derive(Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct ValueWithKeys<K, V>
where
    K: Eq + Hash,
{
    ///the set of keys
    pub keys: HashSet<K>,
    ///the value
    pub value: V,
}

type ValueType<K, V> = Arc<RwLock<ValueWithKeys<K, V>>>;

///A map with multiple keys pointing to one value.
///All keys are unique, but the values may appear multiple times.
///
///Internaly it is a `HashMap` with values being `Arc`s pointing to the value and a set of all keys to the value.
#[derive(Clone)]
pub struct MultiKeyHashMap<K, V>
where
    K: Eq + Hash,
{
    inner: HashMap<K, ValueType<K, V>>,
}

///Iterator over `MultiKeyHashMap`
pub struct ValuesAndKeys<'a, K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    values: hash_map::Values<'a, K, ValueType<K, V>>,
    set: HashSet<*const RwLock<ValueWithKeys<K, V>>>,
}

impl<K, V> MultiKeyHashMap<K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    ///makes a new MultikeyMap
    ///```
    ///use multikeymap::hash::MultiKeyHashMap;
    ///
    ///let map = MultiKeyHashMap::<usize, String>::new();
    ///```
    pub fn new() -> MultiKeyHashMap<K, V> {
        MultiKeyHashMap {
            inner: HashMap::new(),
        }
    }

    ///get a value with all its keys
    ///```
    ///use multikeymap::hash::MultiKeyHashMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///
    ///let mut map = MultiKeyHashMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///
    ///for i in &set1 {
    ///    if let Some(value_with_keys) = map.get_with_keys(i) {
    ///        assert!(value_with_keys.value == "in set 1".to_string());
    ///
    ///        let mut keys = value_with_keys.keys.into_iter().collect::<Vec<usize>>();
    ///        keys.sort();
    ///        assert!(keys == set1.to_owned())
    ///    }
    ///    else {
    ///        panic!("key {} not in map", i)
    ///    }
    ///}
    ///```
    pub fn get_with_keys(&self, k: &K) -> Option<ValueWithKeys<K, V>> {
        Some((self.inner.get(k)?.read().unwrap()).clone())
    }

    ///get a value from a key
    ///```
    ///use multikeymap::hash::MultiKeyHashMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///let set2: [usize; 4] = [5, 10, 20, 30];
    ///
    ///let mut map = MultiKeyHashMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///map.insert(set2, "in set 2".to_string());
    ///
    ///for i in &set1 {
    ///    assert!(map.get(i) == Some("in set 1".to_string()));
    ///}
    ///
    ///for i in &set2 {
    ///    assert!(map.get(i) == Some("in set 2".to_string()));
    ///}
    ///```
    pub fn get(&self, k: &K) -> Option<V> {
        Some(self.inner.get(k)?.read().unwrap().value.clone())
    }

    ///insert all keys from the set `s` and let them point to the value `value`
    ///
    ///if a key is reused it is overwritten
    ///
    ///```
    ///use multikeymap::hash::MultiKeyHashMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///let set2: [usize; 4] = [5, 10, 20, 30];
    ///
    ///let mut map = MultiKeyHashMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///map.insert(set2, "in set 2".to_string());
    ///```  
    pub fn insert<S>(&mut self, s: S, value: V)
    where
        HashSet<K>: From<S>,
    {
        let set: HashSet<K> = HashSet::from(s);
        let a = Arc::new(RwLock::new(ValueWithKeys {
            keys: set.clone(),
            value,
        }));
        for k in set.iter() {
            let old = self.inner.insert((*k).clone(), Arc::clone(&a));
            if let Some(r) = old {
                r.write().unwrap().keys.remove(k);
            }
        }
    }

    ///check if key `k` is in the map
    ///```
    ///use multikeymap::hash::MultiKeyHashMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///let set2: [usize; 4] = [5, 10, 20, 30];
    ///
    ///let mut map = MultiKeyHashMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///map.insert(set2, "in set 2".to_string());
    ///
    ///for i in &set1 {
    ///    assert!(map.contains_key(i));
    ///}
    ///
    ///for i in &set2 {
    ///    assert!(map.contains_key(i));
    ///}
    ///```
    pub fn contains_key(&self, k: &K) -> bool {
        self.inner.contains_key(k)
    }

    ///return an iterator iterating trough each value once
    ///```
    ///use multikeymap::hash::{MultiKeyHashMap, ValueWithKeys};
    ///
    ///let set1: [usize; 4] = [2, 4, 100, 10800];
    ///let set2: [usize; 4] = [5, 101, 201, 3001];
    ///
    ///let mut map = MultiKeyHashMap::<usize, usize>::new();
    ///map.insert(set1, 0);
    ///map.insert(set2, 1);
    ///
    ///for ValueWithKeys{keys, value} in map.values_and_keys() {
    ///    for key in keys {
    ///        assert!(key % 2 == value)
    ///    }
    ///}
    ///```
    pub fn values_and_keys(&self) -> ValuesAndKeys<'_, K, V> {
        ValuesAndKeys {
            values: self.inner.values(),
            set: HashSet::new(),
        }
    }
}

impl<K, V> Default for MultiKeyHashMap<K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<K, V> Iterator for ValuesAndKeys<'_, K, V>
where
    K: Clone + Eq + Hash,
    V: Clone,
{
    type Item = ValueWithKeys<K, V>;
    fn next(&mut self) -> Option<ValueWithKeys<K, V>> {
        loop {
            if let Some(v) = self.values.next() {
                if self.set.contains(&Arc::as_ptr(v)) {
                    continue;
                } else {
                    self.set.insert(Arc::as_ptr(v));
                    break Some(v.read().unwrap().clone());
                }
            } else {
                break None;
            }
        }
    }
}
