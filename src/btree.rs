#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};
use std::{
    collections::{btree_map, BTreeMap, BTreeSet},
    sync::{Arc, RwLock},
};

///entry of `MultiKeyBTreeMap`
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct ValueWithKeys<K, V>
where
    K: Eq + Ord,
{
    ///the set of keys
    pub keys: BTreeSet<K>,
    ///the value
    pub value: V,
}

type ValueType<K, V> = Arc<RwLock<ValueWithKeys<K, V>>>;

///A map with multiple keys pointing to one value.
///All keys are unique, but the values may appear multiple times.
///
///Internaly it is a `BTreeMap` with values being `Arc`s pointing to the value and a set of all keys to the value.
#[derive(Clone)]
pub struct MultiKeyBTreeMap<K, V>
where
    K: Eq + Ord,
{
    inner: BTreeMap<K, ValueType<K, V>>,
}

///Iterator over `MultiKeyBTreeMap`
pub struct ValuesAndKeys<'a, K, V>
where
    K: Eq + Ord + Clone,
    V: Clone,
{
    values: btree_map::Values<'a, K, ValueType<K, V>>,
    set: BTreeSet<*const RwLock<ValueWithKeys<K, V>>>,
}

impl<K, V> MultiKeyBTreeMap<K, V>
where
    K: Eq + Ord + Clone,
    V: Clone,
{
    ///makes a new MultiKeyBTreeMap
    ///```
    ///use multikeymap::btree::MultiKeyBTreeMap;
    ///
    ///let map = MultiKeyBTreeMap::<usize, String>::new();
    ///```
    pub fn new() -> MultiKeyBTreeMap<K, V> {
        MultiKeyBTreeMap {
            inner: BTreeMap::new(),
        }
    }

    ///get a value with all its keys
    ///```
    ///use multikeymap::btree::MultiKeyBTreeMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///
    ///let mut map = MultiKeyBTreeMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///
    ///for i in &set1 {
    ///    if let Some(value_with_keys) = map.get_with_keys(i) {
    ///        assert!(value_with_keys.value == "in set 1".to_string());
    ///
    ///        let mut keys = value_with_keys.keys.into_iter().collect::<Vec<usize>>();
    ///        keys.sort();
    ///        assert!(keys == set1.to_owned())
    ///    }
    ///    else {
    ///        panic!("key {} not in map", i)
    ///    }
    ///}
    ///```
    pub fn get_with_keys(&self, k: &K) -> Option<ValueWithKeys<K, V>> {
        Some((self.inner.get(k)?.read().unwrap()).clone())
    }

    ///get a value from a key
    ///```
    ///use multikeymap::btree::MultiKeyBTreeMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///let set2: [usize; 4] = [5, 10, 20, 30];
    ///
    ///let mut map = MultiKeyBTreeMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///map.insert(set2, "in set 2".to_string());
    ///
    ///for i in &set1 {
    ///    assert!(map.get(i) == Some("in set 1".to_string()));
    ///}
    ///
    ///for i in &set2 {
    ///    assert!(map.get(i) == Some("in set 2".to_string()));
    ///}
    ///```
    pub fn get(&self, k: &K) -> Option<V> {
        Some(self.inner.get(k)?.read().unwrap().value.clone())
    }

    ///insert all keys from the set `s` and let them point to the value `value`
    ///
    ///if a key is reused it is overwritten
    ///
    ///```
    ///use multikeymap::btree::MultiKeyBTreeMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///let set2: [usize; 4] = [5, 10, 20, 30];
    ///
    ///let mut map = MultiKeyBTreeMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///map.insert(set2, "in set 2".to_string());
    ///```  
    pub fn insert<S>(&mut self, s: S, value: V)
    where
        BTreeSet<K>: From<S>,
    {
        let set: BTreeSet<K> = BTreeSet::from(s);
        let a = Arc::new(RwLock::new(ValueWithKeys {
            keys: set.clone(),
            value,
        }));
        for k in set.iter() {
            let old = self.inner.insert((*k).clone(), Arc::clone(&a));
            if let Some(r) = old {
                r.write().unwrap().keys.remove(k);
            }
        }
    }

    ///check if key `k` is in the map
    ///```
    ///use multikeymap::btree::MultiKeyBTreeMap;
    ///
    ///let set1: [usize; 4] = [1, 2, 3, 6];
    ///let set2: [usize; 4] = [5, 10, 20, 30];
    ///
    ///let mut map = MultiKeyBTreeMap::<usize, String>::new();
    ///map.insert(set1, "in set 1".to_string());
    ///map.insert(set2, "in set 2".to_string());
    ///
    ///for i in &set1 {
    ///    assert!(map.contains_key(i));
    ///}
    ///
    ///for i in &set2 {
    ///    assert!(map.contains_key(i));
    ///}
    ///```
    pub fn contains_key(&self, k: &K) -> bool {
        self.inner.contains_key(k)
    }

    ///return an iterator iterating trough each value once
    ///```
    ///use multikeymap::btree::{MultiKeyBTreeMap, ValueWithKeys};
    ///
    ///let set1: [usize; 4] = [2, 4, 100, 10800];
    ///let set2: [usize; 4] = [5, 101, 201, 3001];
    ///
    ///let mut map = MultiKeyBTreeMap::<usize, usize>::new();
    ///map.insert(set1, 0);
    ///map.insert(set2, 1);
    ///
    ///for ValueWithKeys{keys, value} in map.values_and_keys() {
    ///    for key in keys {
    ///        assert!(key % 2 == value)
    ///    }
    ///}
    ///```
    pub fn values_and_keys(&self) -> ValuesAndKeys<'_, K, V> {
        ValuesAndKeys {
            values: self.inner.values(),
            set: BTreeSet::new(),
        }
    }
}

impl<K, V> Default for MultiKeyBTreeMap<K, V>
where
    K: Eq + Ord + Clone,
    V: Clone,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<K, V> Iterator for ValuesAndKeys<'_, K, V>
where
    K: Clone + Eq + Ord,
    V: Clone,
{
    type Item = ValueWithKeys<K, V>;
    fn next(&mut self) -> Option<ValueWithKeys<K, V>> {
        loop {
            if let Some(v) = self.values.next() {
                if self.set.contains(&Arc::as_ptr(v)) {
                    continue;
                } else {
                    self.set.insert(Arc::as_ptr(v));
                    break Some(v.read().unwrap().clone());
                }
            } else {
                break None;
            }
        }
    }
}
