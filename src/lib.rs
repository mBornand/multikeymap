//!# multikeymap
//!A map with multiple keys pointing to one value
//!
//!This crate internally uses `std::collections::HashMap` and is aproximately a wrapper to `HashMap<K, (V, HashSet<K>)>`.
//!
//!## Features
//!- `serde` derives Serialize and Deserialize
pub mod btree;
pub mod hash;
pub use btree::MultiKeyBTreeMap;
pub use hash::MultiKeyHashMap;
